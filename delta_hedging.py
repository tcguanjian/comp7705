from simulation import Simulation2 as Sim2
from simulation import Simulation4 as Sim4
import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt


def call_delta(s, k, maturity, sigma, r):
    d1 = (np.log(s / k) + r * maturity) / (sigma * np.sqrt(maturity)) + sigma * np.sqrt(maturity) / 2
    return norm.cdf(d1)


def play():
    env = Sim2()
    state = env.reset()
    print(state)
    num_of_episode = 2000
    num_frames = (env.num_of_t + 1) * num_of_episode
    all_rewards = []
    episode_reward = 0
    count = 0
    for frame_idx in range(num_frames):
        s = state[1] + env.s
        k = env.k

        maturity = state[2] / 100
        holding = state[0]

        if env.current_num_of_t == env.num_of_t:
            maturity = 0

        # print(s, k, maturity)
        # print('stock price:', s)

        sigma = env.sigma
        r = env.r
        delta = call_delta(s, k, maturity, sigma, r)
        print('delta:', delta)
        action = round(delta * 10)
        if frame_idx % (env.num_of_t + 1) % 2 == 0:
            print('no action')
            action = holding
        print('action:', action)
        next_state, reward, done, discounted_reward = env.step(action)
        print('reward', discounted_reward)
        state = next_state
        episode_reward += discounted_reward
        if done:
            state = env.reset()
            all_rewards.append(episode_reward)
            print('episode_reward', episode_reward)
            episode_reward = 0
            print('done' + str(count))
            count += 1

    print('reward mean:', np.mean(all_rewards))
    print('reward std:', np.std(all_rewards))
    plt.hist(all_rewards, density=True, bins=30)
    plt.ylabel('Probability')
    plt.xlabel('Hedging cost')
    plt.title('Delta Hedging Daily (Brownian Motion)')
    plt.show()
    return np.mean(all_rewards), np.std(all_rewards)


# np.random.seed(12)
play()
