import math, random
from simulation import np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from collections import deque
from simulation import Simulation2 as Sim2
from simulation import Simulation3 as Sim3
from simulation import Simulation4 as Sim4
from simulation import Simulation5 as Sim5
from copy import deepcopy

USE_CUDA = torch.cuda.is_available()


class ModelRing(object):
    def __init__(self, env, capacity):
        self.env = env
        self.model_ring = []
        self.capacity = capacity
        self.head = 0
        for i in range(capacity):
            self.model_ring.append(DQN(env))

    def q(self, state):
        q_values = [model(state) for model in self.model_ring]
        q_values = torch.mean(torch.stack(q_values), dim=0)
        return q_values

    def compute_td_loss(self, batch_size, replay_buffer, gamma):
        temp_model = deepcopy(self)
        for current_model in self.model_ring:
            count = 0
            while count < 10:
                state, action, reward, next_state, done = replay_buffer.sample(batch_size)
                state = torch.FloatTensor(np.float32(state)).cuda()
                next_state = torch.FloatTensor(np.float32(next_state)).cuda()
                action = torch.LongTensor(action).cuda()
                reward = torch.FloatTensor(reward).cuda()
                done = torch.FloatTensor(done).cuda()

                next_q_values = temp_model.q(next_state)

                q_values = current_model(state)
                q_value = q_values.gather(1, action.unsqueeze(1)).squeeze(1)
                next_q_value = next_q_values.max(1)[0]
                expected_q_value = (1 - learning_rate) * q_value + learning_rate * (
                        reward + gamma * next_q_value * (1 - done))
                loss = (q_value - expected_q_value.data).pow(2).mean()

                current_model.optimizer.zero_grad()
                loss.backward()
                current_model.optimizer.step()
                count += 1
        return loss.item()

    def cuda(self):
        for i in range(self.capacity):
            self.model_ring[i] = self.model_ring[i].cuda()

    def act(self, state, epsilon):
        if np.random.rand() > epsilon:
            state = torch.FloatTensor(state).unsqueeze(0).cuda()
            q_value = self.q(state)
            # q_value = self.model_ring[self.head].forward(state)
            action = q_value.max(1)[1].item()
        else:
            action = np.random.randint(self.env.action_space.n)
        return action

    def act2(self, state):
        state = torch.FloatTensor(state).unsqueeze(0).cuda()
        q_value = self.q(state)
        action = q_value.max(1)[1].item()
        max_q_value = q_value.max(1)[0].item()
        return action, max_q_value

    def save(self):
        for i in range(self.capacity):
            torch.save(self.model_ring[i].state_dict(), 'avg_models2/avg_model_' + str(i))

    def load(self):
        for i in range(self.capacity):
            self.model_ring[i].load_state_dict(torch.load('avg_models2/avg_model_' + str(i)))
        self.cuda()


class ReplayBuffer(object):
    def __init__(self, capacity):
        self.buffer = deque(maxlen=capacity)

    def push(self, state, action, reward, next_state, done):
        state = np.expand_dims(state, 0)
        next_state = np.expand_dims(next_state, 0)

        self.buffer.append((state, action, reward, next_state, done))

    def sample(self, batch_size):
        state, action, reward, next_state, done = zip(*random.sample(self.buffer, batch_size))
        return np.concatenate(state), action, reward, np.concatenate(next_state), done

    def __len__(self):
        return len(self.buffer)


class DQN(nn.Module):
    def __init__(self, env):
        super(DQN, self).__init__()

        self.env = env

        self.layers = nn.Sequential(
            nn.Linear(env.observation_space.shape[0], 256),
            nn.ReLU(),
            nn.Linear(256, 256),
            nn.ReLU(),
            nn.Linear(256, 256),
            nn.ReLU(),
            nn.Linear(256, env.action_space.n)
        )

        self.optimizer = optim.Adam(params=self.parameters())

    def forward(self, x):
        return self.layers(x)

    def act(self, state, epsilon):
        if np.random.rand() > epsilon:
            state = torch.FloatTensor(state).unsqueeze(0).cuda()
            q_value = self.forward(state)
            action = q_value.max(1)[1].item()
        else:
            action = np.random.randint(self.env.action_space.n)
        return action

    def act2(self, state):
        state = torch.FloatTensor(state).unsqueeze(0).cuda()
        q_value = self.forward(state)
        action = q_value.max(1)[1].item()
        max_q_value = q_value.max(1)[0].item()
        return action, max_q_value


def plot(rewards, losses):
    plt.figure(figsize=(20, 5))
    plt.subplot(131)
    plt.title('reward')
    plt.plot(rewards)
    plt.subplot(132)
    plt.title('loss')
    plt.plot(losses)
    plt.show()


def train(number_of_models):
    env = Sim5()
    model = ModelRing(env, number_of_models)
    # model.load()

    if USE_CUDA:
        model.cuda()

    replay_buffer = ReplayBuffer(1000)

    num_of_episode = 3000
    batch_size = 100
    gamma = env.gamma
    print('gamma:', gamma)

    epsilon_start = 1
    epsilon_final = 0
    epsilon_decay = 60
    epsilon_by_frame = lambda idx: epsilon_final + (epsilon_start - epsilon_final) * math.exp(
        -1. * idx / epsilon_decay)
    plt.plot([epsilon_by_frame(i) for i in range(num_of_episode)])
    plt.show()
    plt.clf()

    losses = []
    all_rewards = []
    all_cashflows = []
    episode_reward = 0
    episode_cashflow = 0
    state = env.reset()

    beststd = 10

    for idx in range(1, num_of_episode + 1):
        epsilon = epsilon_by_frame(idx)
        print('idx:', idx, 'epsilon', epsilon)

        for i in range((env.num_of_t + 1) * 3):
            action = model.act(state, epsilon)
            next_state, reward, done, discounted_cashflow = env.step(action)
            replay_buffer.push(state, action, reward, next_state, done)

            state = next_state
            episode_reward += reward
            episode_cashflow += discounted_cashflow

            if done:
                state = env.reset()
                all_rewards.append(episode_reward)
                all_cashflows.append(episode_cashflow)
                episode_reward = 0
                episode_cashflow = 0

        if len(replay_buffer) >= batch_size:
            loss = model.compute_td_loss(batch_size, replay_buffer, gamma)
            losses.append(loss)
            print('loss:', loss)
            if idx % 100 == 0 and idx > 400:
                m, std = play(model, env)
                if std < beststd:
                    model.save()
                    beststd = std
            # if idx % 2000 == 0:
            #     plot(all_cashflows, losses)
    plot(all_cashflows, losses)


def play(model, env):
    state = env.reset()
    num_of_episode = 200
    num_frames = (env.num_of_t + 1) * num_of_episode
    all_cashflows = []
    episode_cashflow = 0
    count = 0
    for frame_idx in range(1, num_frames + 1):
        action, max_q_value = model.act2(state)
        next_state, reward, done, cashflow = env.step(action)
        state = next_state
        episode_cashflow += cashflow
        if done:
            state = env.reset()
            all_cashflows.append(episode_cashflow)
            episode_cashflow = 0
            count += 1
    print(all_cashflows)
    print('reward mean:', np.mean(all_cashflows))
    print('reward std:', np.std(all_cashflows))
    return np.mean(all_cashflows), np.std(all_cashflows)


def play2(number_of_models):
    env = Sim4()
    model = ModelRing(env, number_of_models)
    model.load()
    state = env.reset()

    num_of_episode = 2000
    num_frames = (env.num_of_t + 1) * num_of_episode
    all_cashflows = []
    episode_cashflow = 0
    count = 0
    for frame_idx in range(1, num_frames + 1):
        action, max_q_value = model.act2(state)
        # print('action:', action)
        next_state, reward, done, cashflow = env.step(action)
        state = next_state
        episode_cashflow += cashflow
        if done:
            print('done' + str(count))
            state = env.reset()
            all_cashflows.append(episode_cashflow)
            episode_cashflow = 0
            count += 1
    print(all_cashflows)
    print('reward mean:', np.mean(all_cashflows))
    print('reward std:', np.std(all_cashflows))
    plt.hist(all_cashflows, density=True, bins=30)
    plt.ylabel('Probability')
    plt.xlabel('Hedging cost')
    plt.title('Ensemble DQN (Stochastic Volatility)')
    plt.show()
    return np.mean(all_cashflows), np.std(all_cashflows)


learning_rate = 1
number_of_models = 10
# train(number_of_models)
play2(number_of_models)
