import math, random
from simulation import np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt
from collections import deque
from simulation import Simulation2 as Sim2
from simulation import Simulation3 as Sim3
from simulation import Simulation4 as Sim4
from simulation import Simulation5 as Sim5

USE_CUDA = torch.cuda.is_available()


class ReplayBuffer(object):
    def __init__(self, capacity):
        self.buffer = deque(maxlen=capacity)

    def push(self, state, action, reward, next_state, done):
        state = np.expand_dims(state, 0)
        next_state = np.expand_dims(next_state, 0)

        self.buffer.append((state, action, reward, next_state, done))

    def sample(self, batch_size):
        state, action, reward, next_state, done = zip(*random.sample(self.buffer, batch_size))
        return np.concatenate(state), action, reward, np.concatenate(next_state), done

    def __len__(self):
        return len(self.buffer)


class DQN(nn.Module):
    def __init__(self, env):
        super(DQN, self).__init__()

        self.env = env

        self.layers = nn.Sequential(
            nn.Linear(env.observation_space.shape[0], 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, env.action_space.n)
        )

    def forward(self, x):
        return self.layers(x)

    def act(self, state, epsilon):
        if np.random.rand() > epsilon:
            state = torch.FloatTensor(state).unsqueeze(0).cuda()
            q_value = self.forward(state)
            action = q_value.max(1)[1].item()
        else:
            action = np.random.randint(self.env.action_space.n)
        return action

    def act2(self, state):
        state = torch.FloatTensor(state).unsqueeze(0).cuda()
        q_value = self.forward(state)
        action = q_value.max(1)[1].item()
        max_q_value = q_value.max(1)[0].item()
        return action, max_q_value


def act(state, model1, model2, epsilon, env):
    if np.random.rand() > epsilon:
        state = torch.FloatTensor(state).unsqueeze(0).cuda()
        f_values = model1(state) + risk_c * torch.sqrt(torch.abs((model2(state) - model1(state) ** 2)))
        action = f_values.min(1)[1].item()
    else:
        action = np.random.randint(env.action_space.n)
    return action


def act2(state, model1, model2):
    state = torch.FloatTensor(state).unsqueeze(0).cuda()
    f_values = model1(state) + risk_c * torch.sqrt(torch.abs((model2(state) - model1(state) ** 2)))
    action = f_values.min(1)[1]
    q1_values = model1(state)
    q1_value = q1_values.gather(1, action.unsqueeze(1)).squeeze(1)
    q2_values = model2(state)
    q2_value = q2_values.gather(1, action.unsqueeze(1)).squeeze(1)
    return action.item(), q1_value.item(), q2_value.item(), f_values.min().item()


def compute_td_loss(batch_size, replay_buffer, model1, model2, optimizer1, optimizer2, gamma):
    # print('compute_td_loss')

    state, action, reward, next_state, done = replay_buffer.sample(batch_size)

    state = torch.FloatTensor(np.float32(state)).cuda()
    next_state = torch.FloatTensor(np.float32(next_state)).cuda()
    action = torch.LongTensor(action).cuda()
    reward = torch.FloatTensor(reward).cuda()
    done = torch.FloatTensor(done).cuda()

    f_next_values = model1(next_state) + risk_c * torch.sqrt(
        torch.abs((model2(next_state) - model1(next_state) ** 2)))
    next_q_action = f_next_values.min(1)[1]

    q1_values = model1(state)
    q1_value = q1_values.gather(1, action.unsqueeze(1)).squeeze(1)

    next_q1_values = model1(next_state)
    next_q1_value = next_q1_values.gather(1, next_q_action.unsqueeze(1)).squeeze(1)

    expected_q1_value = (1 - learning_rate) * q1_value + learning_rate * (
            (-1 * reward) + gamma * next_q1_value * (1 - done))

    q2_values = model2(state)
    q2_value = q2_values.gather(1, action.unsqueeze(1)).squeeze(1)

    next_q2_values = model2(next_state)
    next_q2_value = next_q2_values.gather(1, next_q_action.unsqueeze(1)).squeeze(1)
    expected_q2_value = (1 - learning_rate2) * q2_value + learning_rate2 * (
            (-1 * reward) ** 2 + (gamma ** 2) * next_q2_value * (1 - done)
            + 2 * (-1 * reward) * gamma * next_q1_value * (1 - done))

    loss1 = (q1_value - expected_q1_value.data).pow(2).mean()
    optimizer1.zero_grad()
    loss1.backward()
    optimizer1.step()
    # print('loss1:', loss1.item())

    loss2 = (q2_value - expected_q2_value.data).pow(2).mean()
    optimizer2.zero_grad()
    loss2.backward()
    optimizer2.step()
    # print('loss2:', loss2.item())

    return loss1.item(), loss2.item()


def plot(rewards, losses):
    plt.figure(figsize=(20, 5))
    plt.subplot(131)
    plt.title('reward')
    plt.plot(rewards)
    plt.subplot(132)
    plt.title('loss')
    plt.plot(losses)
    plt.show()


def train():
    env = Sim5()

    model1 = DQN(env)
    model2 = DQN(env)

    if USE_CUDA:
        model1 = model1.cuda()
        model2 = model2.cuda()

    optimizer1 = optim.Adam(params=model1.parameters(), lr=0.00001)
    optimizer2 = optim.Adam(params=model2.parameters(), lr=0.00001)

    replay_buffer = ReplayBuffer(2000)

    num_of_episode = 10000
    batch_size = 500
    gamma = env.gamma
    print('gamma:', gamma)

    epsilon_start = 1.0
    epsilon_final = 0
    epsilon_decay = 20
    epsilon_by_frame = lambda frame_idx: epsilon_final + (epsilon_start - epsilon_final) * math.exp(
        -1. * frame_idx / epsilon_decay)
    plt.plot([epsilon_by_frame(i) for i in range(num_of_episode)])
    plt.show()
    plt.clf()

    losses = []
    all_rewards = []
    episode_reward = 0
    state = env.reset()

    beststd = 1
    for idx in range(1, num_of_episode + 1):
        print('idx:', idx)
        epsilon = epsilon_by_frame(idx)

        for i in range(env.num_of_t + 1):
            action = act(state, model1, model2, epsilon, env)
            # if idx % 10 == 0:
            #     print('train action:', action)
            next_state, reward, done, discounted_reward = env.step(action)
            replay_buffer.push(state, action, reward, next_state, done)
            state = next_state
            episode_reward += discounted_reward

            if done:
                state = env.reset()
                all_rewards.append(episode_reward)
                # print('episode_cashflow', episode_reward)
                episode_reward = 0

            if len(replay_buffer) >= batch_size:
                loss1, loss2 = compute_td_loss(batch_size, replay_buffer, model1, model2, optimizer1, optimizer2, gamma)
                losses.append(loss2)
                # print('loss1:', loss1)
                # print('loss2:', loss2)

        if idx > 200 and idx % 100 == 0:
            m, std = play(model1, model2, env)
            if std < beststd:
                torch.save(model1.state_dict(), 'model1.pkl')
                torch.save(model2.state_dict(), 'model2.pkl')
                beststd = std

    plot(all_rewards, losses)


def load_model(env):
    model1 = DQN(env)
    model2 = DQN(env)
    model1.load_state_dict(torch.load('model1.pkl'))
    model2.load_state_dict(torch.load('model2.pkl'))
    model1.cuda()
    model2.cuda()
    return model1, model2


def play(model1, model2, env):
    state = env.reset()
    num_of_episode = 100
    num_frames = (env.num_of_t + 1) * num_of_episode
    all_rewards = []
    episode_reward = 0
    count = 0
    for frame_idx in range(1, num_frames + 1):
        action, _, _, _ = act2(state, model1, model2)
        next_state, reward, done, discounted_reward = env.step(action)
        state = next_state
        episode_reward += discounted_reward
        if done:
            state = env.reset()
            all_rewards.append(episode_reward)
            episode_reward = 0
            print('done' + str(count))
            count += 1

    print('reward mean:', np.mean(all_rewards))
    print('reward std:', np.std(all_rewards))
    return np.mean(all_rewards), np.std(all_rewards)


def play2():
    env = Sim4()
    model1, model2 = load_model(env)
    state = env.reset()
    num_of_episode = 2000
    num_frames = (env.num_of_t + 1) * num_of_episode
    all_rewards = []
    episode_reward = 0
    count = 0
    for frame_idx in range(1, num_frames + 1):
        action, _, _, _ = act2(state, model1, model2)
        # print('action:', action)
        next_state, reward, done, discounted_reward = env.step(action)
        state = next_state
        episode_reward += discounted_reward
        if done:
            state = env.reset()
            all_rewards.append(episode_reward)
            episode_reward = 0
            print('done' + str(count))
            count += 1

    print('reward mean:', np.mean(all_rewards))
    print('reward std:', np.std(all_rewards))
    plt.hist(all_rewards, density=True, bins=30)
    plt.ylabel('Probability')
    plt.xlabel('Hedging cost')
    plt.title('Jay’s method (Stochastic Volatility)')
    plt.show()
    return np.mean(all_rewards), np.std(all_rewards)


learning_rate = 0.5
learning_rate2 = 0.5
risk_c = 0.5
# train()
play2()
