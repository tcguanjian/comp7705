import math, random
import gym
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.autograd as autograd
import matplotlib.pyplot as plt
from collections import deque
from simulation import Simulation as Sim
from simulation import Simulation1 as Sim1
from simulation import Simulation2 as Sim2
from simulation import Simulation4 as Sim4

USE_CUDA = torch.cuda.is_available()
learning_rate = 0.5


class ReplayBuffer(object):
    def __init__(self, capacity):
        self.buffer = deque(maxlen=capacity)

    def push(self, state, action, reward, next_state, done):
        state = np.expand_dims(state, 0)
        next_state = np.expand_dims(next_state, 0)

        self.buffer.append((state, action, reward, next_state, done))

    def sample(self, batch_size):
        state, action, reward, next_state, done = zip(*random.sample(self.buffer, batch_size))
        return np.concatenate(state), action, reward, np.concatenate(next_state), done

    def __len__(self):
        return len(self.buffer)


class DQN(nn.Module):
    def __init__(self, env):
        super(DQN, self).__init__()

        self.env = env

        self.layers = nn.Sequential(
            nn.Linear(env.observation_space.shape[0], 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, env.action_space.n)
        )

    def forward(self, x):
        return self.layers(x)

    def act(self, state, epsilon):
        if random.random() > epsilon:
            state = torch.FloatTensor(state).unsqueeze(0).cuda()
            q_value = self.forward(state)
            action = q_value.max(1)[1].item()
        else:
            action = random.randrange(self.env.action_space.n)
        return action

    def act2(self, state):
        state = torch.FloatTensor(state).unsqueeze(0).cuda()
        q_value = self.forward(state)
        action = q_value.max(1)[1].item()
        max_q_value = q_value.max(1)[0].item()
        return action, max_q_value


def compute_td_loss(batch_size, replay_buffer, model, optimizer, gamma):
    state, action, reward, next_state, done = replay_buffer.sample(batch_size)

    state = torch.FloatTensor(np.float32(state)).cuda()
    next_state = torch.FloatTensor(np.float32(next_state)).cuda()
    action = torch.LongTensor(action).cuda()
    reward = torch.FloatTensor(reward).cuda()
    done = torch.FloatTensor(done).cuda()

    q_values = model(state)
    next_q_values = model(next_state)

    q_value = q_values.gather(1, action.unsqueeze(1)).squeeze(1)
    next_q_value = next_q_values.max(1)[0]
    expected_q_value = (1 - learning_rate) * q_value + learning_rate * (reward + gamma * next_q_value * (1 - done))

    loss = (q_value - expected_q_value.data).pow(2).mean()

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    return loss.item()


def plot(frame_idx, rewards, losses):
    plt.figure(figsize=(20, 5))
    plt.subplot(131)
    plt.title('frame %s. reward: %s' % (frame_idx, np.mean(rewards[-10:])))
    plt.plot(rewards)
    plt.subplot(132)
    plt.title('loss')
    plt.plot(losses)
    plt.show()


def train(risk_c):
    env = Sim1(risk_c)

    model = DQN(env)

    if USE_CUDA:
        model = model.cuda()

    optimizer = optim.Adam(model.parameters(), lr=0.00001)

    replay_buffer = ReplayBuffer(2000)

    num_of_episode = 30000
    num_frames = (env.num_of_t + 1) * num_of_episode
    batch_size = 500
    gamma = env.gamma
    print('gamma:', gamma)

    epsilon_start = 1.0
    epsilon_final = 0.0
    epsilon_decay = 100
    epsilon_by_frame = lambda frame_idx: epsilon_final + (epsilon_start - epsilon_final) * math.exp(
        -1. * frame_idx / epsilon_decay)
    plt.plot([epsilon_by_frame(i) for i in range(num_frames)])
    plt.show()
    plt.clf()

    losses = []
    all_rewards = []
    episode_reward = 0
    state = env.reset()

    episode_num = 0
    beststd = 1
    for frame_idx in range(1, num_frames + 1):
        epsilon = epsilon_by_frame(frame_idx)
        action = model.act(state, epsilon)

        next_state, reward, done, P_and_L = env.step(action)
        # print(next_state, reward, done)
        replay_buffer.push(state, action, reward, next_state, done)

        state = next_state
        episode_reward += P_and_L

        if done:
            episode_num += 1
            if episode_num % 10 == 0:
                print('episode_num:', episode_num)
                print('frame_idx:', frame_idx)
            state = env.reset()
            all_rewards.append(episode_reward)
            # print(episode_reward)
            episode_reward = 0

        if len(replay_buffer) > batch_size:
            loss = compute_td_loss(batch_size, replay_buffer, model, optimizer, gamma)
            losses.append(loss)
            if frame_idx % 3100 == 0 and frame_idx > 0:
                m, std = play2(model, env)
                if std < beststd:
                    torch.save(model.state_dict(), 'model.pkl')
                    if std < 0.35:
                        break
                    beststd = std

    plot(frame_idx, all_rewards, losses)


def load_model(env):
    model = DQN(env)
    model.load_state_dict(torch.load('model.pkl'))
    model.cuda()
    return model


def play2(model, env):
    state = env.reset()

    num_of_episode = 200
    num_frames = (env.num_of_t + 1) * num_of_episode
    all_rewards = []
    episode_reward = 0

    for frame_idx in range(1, num_frames + 1):
        action, max_q_value = model.act2(state)
        next_state, reward, done, P_and_L = env.step(action)
        state = next_state
        episode_reward += P_and_L
        if done:
            state = env.reset()
            all_rewards.append(episode_reward)
            episode_reward = 0
            print('done')
    print('reward mean:', np.mean(all_rewards))
    print('reward std:', np.std(all_rewards))
    return np.mean(all_rewards), np.std(all_rewards)


def play():
    env = Sim4()
    model = load_model(env)
    state = env.reset()

    num_of_episode = 2000
    num_frames = (env.num_of_t + 1) * num_of_episode
    all_rewards = []
    episode_reward = 0

    for frame_idx in range(1, num_frames + 1):
        action, max_q_value = model.act2(state)
        next_state, reward, done, P_and_L = env.step(action)
        state = next_state
        episode_reward += P_and_L
        if done:
            state = env.reset()
            all_rewards.append(episode_reward)
            episode_reward = 0
            print('done')
    print('reward mean:', np.mean(all_rewards))
    print('reward std:', np.std(all_rewards))
    plt.hist(all_rewards, density=True, bins=30)
    plt.ylabel('Probability')
    plt.xlabel('Hedging cost')
    plt.title('Kolm’s method (Stochastic Volatility)')
    plt.show()
    return np.mean(all_rewards), np.std(all_rewards)


risk_c = 0.6
# train(risk_c)
play()
