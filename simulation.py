import numpy as np
from scipy.stats import norm
import gym
from gym import spaces
import time


def black_scholes_price(s, k, maturity, sigma, r, option_type):
    if option_type == 'call':
        if maturity == 0:
            return max(s - k, 0)
    if option_type == 'put':
        if maturity == 0:
            return max(k - s, 0)
    d1 = (np.log(s / k) + r * maturity) / (sigma * np.sqrt(maturity)) + sigma * np.sqrt(maturity) / 2
    d2 = d1 - sigma * np.sqrt(maturity)
    if option_type == 'call':
        return s * norm.cdf(d1) - k * np.exp(-1 * r * maturity) * norm.cdf(d2)
    elif option_type == 'put':
        return k * np.exp(-1 * r * maturity) * norm.cdf(-1 * d2) - s * norm.cdf(-1 * d1)
    else:
        return None


# P&L special reward for ritterkolm's model (brownian motion)
class Simulation(gym.Env):

    def __init__(self, risk_c):
        super(Simulation, self).__init__()
        seed = time.time_ns() % (2 ** 32)
        np.random.seed(seed)

        self.risk_c = risk_c

        # 0 to 10
        self.action_space = spaces.Discrete(11)
        self.observation_space = spaces.Box(low=-10000.0, high=10000.0, shape=(3,), dtype=np.float32)

        # option specification
        self.s = 40
        self.k = 40
        self.r = 0.0
        self.maturity = 30 / 365
        self.sigma = 0.2

        self.current_c = black_scholes_price(self.s, self.k, self.maturity, self.sigma, self.r, 'call')
        self.last_c = black_scholes_price(self.s, self.k, self.maturity, self.sigma, self.r, 'call')

        self.s_mu = 0.05

        # number of small t
        self.num_of_t = 30
        self.small_t = self.maturity / self.num_of_t
        self.current_num_of_t = 0
        self.done = False

        self.current_holding = 0
        self.current_s = self.s
        self.last_s = self.s
        self.current_maturity = self.maturity

        self.gamma = 1

        # 1%
        self.cost_k = 0.01

    def step(self, action):
        if self.current_num_of_t == self.num_of_t:
            self.done = True
        else:
            self.done = False

        if self.done:
            new_holding = 0
        else:
            new_holding = 0.1 * action

        # current option price
        self.last_c = self.current_c
        if self.done:
            self.current_maturity = 0
        self.current_c = black_scholes_price(self.current_s, self.k, self.current_maturity, self.sigma, self.r, 'call')
        P_and_L_1 = self.last_c - self.current_c
        P_and_L_2 = self.current_holding * (self.current_s - self.last_s)
        P_and_L_3 = -1 * np.abs(new_holding - self.current_holding) * self.current_s * self.cost_k

        new_s = self.current_s * np.exp((self.s_mu - np.square(self.sigma) / 2) * self.small_t + self.sigma * np.sqrt(
            self.small_t) * np.random.normal())
        self.last_s = self.current_s
        self.current_s = new_s
        self.current_holding = new_holding
        self.current_maturity = self.current_maturity - self.small_t

        P_and_L = P_and_L_1 + P_and_L_2 + P_and_L_3
        reward = P_and_L - self.risk_c * np.square(P_and_L)

        next_state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        self.current_num_of_t += 1
        return next_state, reward, self.done, P_and_L

    def reset(self):
        self.__init__(self.risk_c)
        state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        return state

    def render(self, mode='human'):
        pass

    def close(self):
        pass


# P&L special reward for ritterkolm's model (Stochastic Volatility)
class Simulation1(gym.Env):

    def __init__(self, risk_c):
        super(Simulation1, self).__init__()
        seed = time.time_ns() % (2 ** 32)
        np.random.seed(seed)

        self.risk_c = risk_c

        # 0 to 10
        self.action_space = spaces.Discrete(11)
        self.observation_space = spaces.Box(low=-10000.0, high=10000.0, shape=(3,), dtype=np.float32)

        # option specification
        self.s = 40
        self.k = 40
        self.r = 0.0
        self.maturity = 30 / 365
        self.sigma = 0.2

        self.current_c = black_scholes_price(self.s, self.k, self.maturity, self.sigma, self.r, 'call')
        self.last_c = black_scholes_price(self.s, self.k, self.maturity, self.sigma, self.r, 'call')

        self.s_mu = 0.05

        # number of small t
        self.num_of_t = 30
        self.small_t = self.maturity / self.num_of_t
        self.current_num_of_t = 0
        self.done = False

        self.current_holding = 0
        self.current_s = self.s
        self.last_s = self.s
        self.current_maturity = self.maturity

        self.current_sigma = self.sigma
        self.correlation = -0.4
        self.vol_of_vol = 0.6

        self.gamma = 1

        # 1%
        self.cost_k = 0.01

    def step(self, action):
        if self.current_num_of_t == self.num_of_t:
            self.done = True
        else:
            self.done = False

        if self.done:
            new_holding = 0
        else:
            new_holding = 0.1 * action

        # current option price
        self.last_c = self.current_c
        if self.done:
            self.current_maturity = 0
        self.current_c = black_scholes_price(self.current_s, self.k, self.current_maturity, self.sigma, self.r, 'call')
        P_and_L_1 = self.last_c - self.current_c
        P_and_L_2 = self.current_holding * (self.current_s - self.last_s)
        P_and_L_3 = -1 * np.abs(new_holding - self.current_holding) * self.current_s * self.cost_k


        self.last_s = self.current_s

        n = 10
        small_small_t = self.small_t / n
        for j in range(n):
            dz1, dz2 = np.random.multivariate_normal([0, 0],
                                                     [[small_small_t, self.correlation * small_small_t],
                                                      [self.correlation * small_small_t, small_small_t]],
                                                     1).T
            dz1 = dz1[0]
            dz2 = dz2[0]
            # print('dz1dz2', dz1, dz1)
            self.current_s = self.current_s + self.s_mu * self.current_s * small_small_t + self.current_sigma * self.current_s * dz1
            self.current_sigma = self.current_sigma + self.vol_of_vol * self.current_sigma * dz2

        self.current_holding = new_holding
        self.current_maturity = self.current_maturity - self.small_t

        P_and_L = P_and_L_1 + P_and_L_2 + P_and_L_3
        reward = P_and_L - self.risk_c * np.square(P_and_L)

        next_state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        self.current_num_of_t += 1
        return next_state, reward, self.done, P_and_L

    def reset(self):
        self.__init__(self.risk_c)
        state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        return state

    def render(self, mode='human'):
        pass

    def close(self):
        pass


# cashflow reward for evaluation (brownian motion)
class Simulation2(gym.Env):

    def __init__(self):
        super(Simulation2, self).__init__()

        seed = time.time_ns() % (2 ** 32)
        np.random.seed(seed)

        # 0 to 10
        self.action_space = spaces.Discrete(11)
        self.observation_space = spaces.Box(low=-10000.0, high=10000.0, shape=(3,), dtype=np.float32)

        # option specification
        self.s = 40
        self.k = 40
        self.r = 0.0
        self.maturity = 30 / 365
        self.sigma = 0.2

        self.s_mu = 0.05

        # number of small t
        self.num_of_t = 30
        self.small_t = self.maturity / self.num_of_t
        self.current_num_of_t = 0
        self.done = False

        self.current_holding = 0
        self.current_s = self.s
        self.current_maturity = self.maturity

        self.gamma = np.exp(-1 * self.r * self.small_t)

        # 1%
        self.cost_k = 0.01

    def step(self, action):
        if self.current_num_of_t == self.num_of_t:
            self.done = True
        else:
            self.done = False

        if self.done:
            new_holding = 0
        else:
            new_holding = 0.1 * action
        in_cashflow1 = -1 * (new_holding - self.current_holding) * self.current_s
        in_cashflow2 = -1 * np.abs(new_holding - self.current_holding) * self.current_s * self.cost_k
        in_cashflow = in_cashflow1 + in_cashflow2

        if self.done:
            in_cashflow -= max(self.current_s - self.k, 0)

        new_s = self.current_s * np.exp((self.s_mu - np.square(self.sigma) / 2) * self.small_t + self.sigma * np.sqrt(
            self.small_t) * np.random.normal())

        self.current_s = new_s
        self.current_holding = new_holding
        self.current_maturity = self.current_maturity - self.small_t

        reward = in_cashflow
        next_state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        discount = np.exp(-1 * self.r * self.small_t * (self.current_num_of_t))
        self.current_num_of_t += 1
        return next_state, reward, self.done, discount * reward

    def reset(self):
        self.__init__()
        state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        return state

    def render(self, mode='human'):
        pass

    def close(self):
        pass


# P&L reward for johnhull's model, ensemble dqn (brownian motion)
class Simulation3(gym.Env):

    def __init__(self):
        super(Simulation3, self).__init__()

        seed = time.time_ns() % (2 ** 32)
        np.random.seed(seed)

        # 0 to 10
        self.action_space = spaces.Discrete(11)
        self.observation_space = spaces.Box(low=-10000.0, high=10000.0, shape=(3,), dtype=np.float32)

        # option specification
        self.s = 40
        self.k = 40
        self.r = 0.0
        self.maturity = 30 / 365
        self.sigma = 0.2

        # option price
        self.current_c = black_scholes_price(self.s, self.k, self.maturity, self.sigma, self.r, 'call')
        self.last_c = self.current_c

        self.s_mu = 0.05

        # number of small t
        self.num_of_t = 30
        self.small_t = self.maturity / self.num_of_t
        self.current_num_of_t = 0
        self.done = False

        self.current_holding = 0
        self.current_s = self.s
        self.last_s = self.s
        self.current_maturity = self.maturity

        self.gamma = 1

        # 1%
        self.cost_k = 0.01

    def step(self, action):
        if self.current_num_of_t == self.num_of_t:
            self.done = True
        else:
            self.done = False

        if self.done:
            new_holding = 0
        else:
            new_holding = 0.1 * action

        # current option price
        self.last_c = self.current_c
        if self.done:
            self.current_maturity = 0
        self.current_c = black_scholes_price(self.current_s, self.k, self.current_maturity, self.sigma, self.r, 'call')

        P_and_L_1 = self.last_c - self.current_c
        P_and_L_2 = self.current_holding * (self.current_s - self.last_s)
        P_and_L_3 = -1 * np.abs(new_holding - self.current_holding) * self.current_s * self.cost_k

        new_s = self.current_s * np.exp((self.s_mu - np.square(self.sigma) / 2) * self.small_t + self.sigma * np.sqrt(
            self.small_t) * np.random.normal())
        self.last_s = self.current_s
        self.current_s = new_s
        self.current_holding = new_holding
        self.current_maturity = self.current_maturity - self.small_t

        P_and_L = P_and_L_1 + P_and_L_2 + P_and_L_3

        reward = P_and_L

        next_state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        self.current_num_of_t += 1
        return next_state, reward, self.done, P_and_L

    def reset(self):
        self.__init__()
        state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        return state

    def render(self, mode='human'):
        pass

    def close(self):
        pass


# cashflow reward for evaluation (Stochastic Volatility)
class Simulation4(gym.Env):

    def __init__(self):
        super(Simulation4, self).__init__()

        seed = time.time_ns() % (2 ** 32)
        np.random.seed(seed)

        # 0 to 10
        self.action_space = spaces.Discrete(11)
        self.observation_space = spaces.Box(low=-10000.0, high=10000.0, shape=(3,), dtype=np.float32)

        # option specification
        self.s = 40
        self.k = 40
        self.r = 0.0
        self.maturity = 30 / 365
        self.sigma = 0.2

        # option price
        self.current_c = black_scholes_price(self.s, self.k, self.maturity, self.sigma, self.r, 'call')
        self.last_c = self.current_c

        self.s_mu = 0.05

        # number of small t
        self.num_of_t = 30
        self.small_t = self.maturity / self.num_of_t
        self.current_num_of_t = 0
        self.done = False

        self.current_holding = 0
        self.current_s = self.s
        self.current_maturity = self.maturity

        self.current_sigma = self.sigma
        self.correlation = -0.4
        self.vol_of_vol = 0.6

        self.gamma = 1

        # 1%
        self.cost_k = 0.01

    def step(self, action):
        if self.current_num_of_t == self.num_of_t:
            self.done = True
        else:
            self.done = False

        if self.done:
            new_holding = 0
        else:
            new_holding = 0.1 * action

        in_cashflow1 = -1 * (new_holding - self.current_holding) * self.current_s
        in_cashflow2 = -1 * np.abs(new_holding - self.current_holding) * self.current_s * self.cost_k
        in_cashflow = in_cashflow1 + in_cashflow2

        if self.done:
            in_cashflow -= max(self.current_s - self.k, 0)

        # print('current vol:', self.current_sigma)
        n = 10
        small_small_t = self.small_t / n
        for j in range(n):
            dz1, dz2 = np.random.multivariate_normal([0, 0],
                                                     [[small_small_t, self.correlation * small_small_t],
                                                      [self.correlation * small_small_t, small_small_t]],
                                                     1).T
            dz1 = dz1[0]
            dz2 = dz2[0]
            # print('dz1dz2', dz1, dz1)
            self.current_s = self.current_s + self.s_mu * self.current_s * small_small_t + self.current_sigma * self.current_s * dz1
            self.current_sigma = self.current_sigma + self.vol_of_vol * self.current_sigma * dz2

        self.current_holding = new_holding
        self.current_maturity = self.current_maturity - self.small_t

        reward = in_cashflow

        next_state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        discount = np.exp(-1 * self.r * self.small_t * (self.current_num_of_t))
        self.current_num_of_t += 1
        return next_state, reward, self.done, reward * discount

    def reset(self):
        self.__init__()
        state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        return state

    def render(self, mode='human'):
        pass

    def close(self):
        pass


# P&L reward for johnhull's model, ensemble dqn (Stochastic Volatility)
class Simulation5(gym.Env):

    def __init__(self):
        super(Simulation5, self).__init__()

        seed = time.time_ns() % (2 ** 32)
        np.random.seed(seed)

        # 0 to 10
        self.action_space = spaces.Discrete(11)
        self.observation_space = spaces.Box(low=-10000.0, high=10000.0, shape=(3,), dtype=np.float32)

        # option specification
        self.s = 40
        self.k = 40
        self.r = 0.0
        self.maturity = 30 / 365
        self.sigma = 0.2

        # option price
        self.current_c = black_scholes_price(self.s, self.k, self.maturity, self.sigma, self.r, 'call')
        self.last_c = self.current_c

        self.s_mu = 0.05

        # number of small t
        self.num_of_t = 30
        self.small_t = self.maturity / self.num_of_t
        self.current_num_of_t = 0
        self.done = False

        self.current_holding = 0
        self.current_s = self.s
        self.last_s = self.s
        self.current_maturity = self.maturity

        self.current_sigma = self.sigma
        self.correlation = -0.4
        self.vol_of_vol = 0.6

        self.gamma = 1

        # 1%
        self.cost_k = 0.01

    def step(self, action):
        if self.current_num_of_t == self.num_of_t:
            self.done = True
        else:
            self.done = False

        if self.done:
            new_holding = 0
        else:
            new_holding = 0.1 * action

        # current option price
        self.last_c = self.current_c
        if self.done:
            self.current_maturity = 0
        self.current_c = black_scholes_price(self.current_s, self.k, self.current_maturity, self.sigma, self.r, 'call')

        P_and_L_1 = self.last_c - self.current_c
        P_and_L_2 = self.current_holding * (self.current_s - self.last_s)
        P_and_L_3 = -1 * np.abs(new_holding - self.current_holding) * self.current_s * self.cost_k
        self.last_s = self.current_s
        # print('current vol:', self.current_sigma)
        n = 10
        small_small_t = self.small_t / n
        for j in range(n):
            dz1, dz2 = np.random.multivariate_normal([0, 0],
                                                     [[small_small_t, self.correlation * small_small_t],
                                                      [self.correlation * small_small_t, small_small_t]],
                                                     1).T
            dz1 = dz1[0]
            dz2 = dz2[0]
            # print('dz1dz2', dz1, dz1)
            self.current_s = self.current_s + self.s_mu * self.current_s * small_small_t + self.current_sigma * self.current_s * dz1
            self.current_sigma = self.current_sigma + self.vol_of_vol * self.current_sigma * dz2

        self.current_holding = new_holding
        self.current_maturity = self.current_maturity - self.small_t

        P_and_L = P_and_L_1 + P_and_L_2 + P_and_L_3

        reward = P_and_L

        next_state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        self.current_num_of_t += 1
        return next_state, reward, self.done, P_and_L

    def reset(self):
        self.__init__()
        state = np.asarray([self.current_holding * 10, self.current_s - self.s, self.current_maturity * 100])
        return state

    def render(self, mode='human'):
        pass

    def close(self):
        pass
